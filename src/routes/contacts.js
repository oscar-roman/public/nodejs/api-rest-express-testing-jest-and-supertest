const { Router } = require('express');
const router = Router();
const { fakeDatabase } = require('../database/fakeDatabase');
const {
    isNumberWitOutDecimals
  } = require('../httpHelpers');
  
//   return all contacts or filtered
router.get('/contacts',(req,res)=>{
    var contacts = fakeDatabase.selectAllFromContacts();
    const { phrase, limit} = req.query;
    if(Object.keys(req.query).length <= 0){
      res.status(200).json(contacts);
    }else{
      if(phrase == '' || limit == '')
        res.status(400).end();
      else{
        if(phrase && limit){
            let filterContacts = searchByName(contacts,phrase);
            let contactsLimitFiltered = filterContacts.length > 0 ? limitContacts(filterContacts,parseInt(limit)) : filterContacts;
            res.status(200).json(contactsLimitFiltered);
        }else
        if(phrase){
          res.status(200).json(searchByName(contacts, phrase));
        }
        else
        if(limit && (limit.length > 0 && parseInt(limit) >= 0 && isNumberWitOutDecimals(limit)))
            res.status(200).json(limitContacts(contacts,parseInt(limit)));
        else
            res.status(400).end();
      }
    }
})

// return contact by id
router.get('/contacts/:id',(req,res)=>{
    const { id } = req.params;
    let users = fakeDatabase.selectFromContactsById(id);
    users.length > 0 ? res.status(200).json(users[0]) : res.status(404).end();
});

//delete user by id
router.delete('/contacts/:id',(req,res)=>{
    const { id } = req.params;
    let users = fakeDatabase.selectFromContactsById(id);
    if(users.length > 0){
        fakeDatabase.deleteContactsById(id);
        res.status(204).end();
    }else
        res.status(400).end();
});

function orderByName(contacts){
    return contacts.sort(function(a, b){
      if(a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
      if(a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
      return 0;
    })
}
  
function searchByName(contacts, phrase){
    contacts = contacts.filter((contact) => {
      return contact.name.toLowerCase().includes(phrase.toLowerCase());
    })
  return orderByName(contacts);
}

function limitContacts(contacts,limit){
  let result = contacts.slice(0,limit);
  return result;
}

module.exports = router;