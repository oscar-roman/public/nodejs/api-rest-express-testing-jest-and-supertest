const { Router } = require('express');
const router = Router();

router.get('/ping',(req,res)=>{
    res.status(200)
    .set('Content-Type', 'text/plain')
    .send('pong');
});

module.exports = router;