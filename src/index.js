const { fakeDatabase } = require('./database/fakeDatabase');
const express  = require('express');
const morgan = require('morgan');
const contacts = require('./database/fakeContactsData.json');
const app = express();

contacts.forEach((contact) => {
  fakeDatabase.insertIntoContacts(contact);
});


// routes
app.use(require('./routes/index'));
app.use(require('./routes/contacts'));
app.use(require('./routes/ping'));

// middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false }));
app.use(express.json());
app.use(function(req, res, next) {
  res.status(404).end();
})


module.exports = app;
