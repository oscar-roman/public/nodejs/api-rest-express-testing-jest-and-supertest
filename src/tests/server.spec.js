const request = require('supertest');
const app = require('../index');

describe('server', () => {
  beforeEach(() => {
  });

  test('404 Not Found is returned for unexpected endpoint', async () => {
    await request(app)
      .get('/non-existent-endpoint-route')
      .expect(404);
  });
});
