const { expectEqualContactsIgnoringOrder } = require('../custom-assertions');
const { contactFixture } = require('../fixtures');
const { fakeDatabase } = require('../../database/fakeDatabase');
const request = require('supertest');
const app = require('../../index');

describe('DELETE /contacts/<contact-id>', () => {

  beforeEach(() => {
    fakeDatabase.clear();
  });

  test('delete Contact by its ID', async () => {
    // given
    const contact1 = contactFixture({ id: '111' });
    const contact2 = contactFixture({ id: '222' });
    const contact3 = contactFixture({ id: '333' });
    fakeDatabase.insertIntoContacts(contact1);
    fakeDatabase.insertIntoContacts(contact2);
    fakeDatabase.insertIntoContacts(contact3);

    await request(app).delete('/contacts/222').expect(204);
    expectEqualContactsIgnoringOrder(fakeDatabase.data().contacts, [
      contact1,
      contact3,
    ]);
     
    
  });
});
