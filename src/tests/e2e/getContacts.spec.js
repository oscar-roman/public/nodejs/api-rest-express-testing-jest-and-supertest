const { expectEqualContactsIgnoringOrder } = require('../custom-assertions');
const { contactFixture } = require('../fixtures');
const { fakeDatabase } = require('../../database/fakeDatabase');
const request = require('supertest');
const app = require('../../index');

let pageName = '/contacts';

describe('GET /contacts', () => {
  beforeEach(() => {
    fakeDatabase.clear();
  });
  
  test('get all Contacts', async () => {
      // given
      const contact1 = contactFixture({
        id: '123',
        name: 'Contact 123',
        phone: '111-222-333',
        addressLines: ['Fancy Street', 'Mega City', 'What-a-Country'],
      });
      const contact2 = contactFixture({
        id: '234',
        name: 'Contact 234',
        phone: '222-333-444',
        addressLines: ['Lake Cottage', 'Waters', 'Sealand'],
      });
      fakeDatabase.insertIntoContacts(contact1);
      fakeDatabase.insertIntoContacts(contact2);
  
      await ExpectedRequest(pageName, (res) => {
          expectEqualContactsIgnoringOrder(res.body, [contact1, contact2]);
        });
    });
  
  test('Contacts are sorted by name', async () => {
      // given
      const contactA = contactFixture({
          id: 'A',
          name: 'name A',
      });
      const contactB = contactFixture({
          id: 'B',
          name: 'name B',
      });
      fakeDatabase.insertIntoContacts(contactA);
      fakeDatabase.insertIntoContacts(contactB);
  
      await ExpectedRequest(pageName, [contactA, contactB])
  });
  
  test('number of returned Contacts can be limited to a requested number', async () => {
    // given
    const contactA = contactFixture({ name: 'A' });
    const contactB = contactFixture({ name: 'B' });
    const contactC = contactFixture({ name: 'C' });
    fakeDatabase.insertIntoContacts(contactA);
    fakeDatabase.insertIntoContacts(contactB);
    fakeDatabase.insertIntoContacts(contactC);
    await ExpectedRequest(`${pageName}?limit=2`, [contactA, contactB]);
  });
  
  ['-1', '2.3', 'x'].forEach((invalidLimit) => {
    test(`invalid limit for returned Contacts results with 400 Bad Request (case: "${invalidLimit}")`, async () => {
      await ExpectedRequest(`/contacts?limit=${invalidLimit}`,400)
    });
  });
  
  ['X', 'ddd', 'e f'].forEach((phrase) => {
    test(`Contacts can filtered by a name part phrase (case "${phrase}")`, async () => {
      // given
      const matchingContact = contactFixture({ name: 'aXc ddd e f' });
      const nonMatchingContact = contactFixture({ name: 'ac dd e  f' });
      fakeDatabase.insertIntoContacts(matchingContact);
      fakeDatabase.insertIntoContacts(nonMatchingContact);
  
      await ExpectedRequest(`${pageName}?phrase=${phrase}`,[matchingContact])
    });
  });
  
  test(`empty Contact name phrase results with 400 Bad Request`, async () => {
    // given any Contacts
    fakeDatabase.insertIntoContacts(contactFixture());
    fakeDatabase.insertIntoContacts(contactFixture());
  
    ExpectedRequest(`${pageName}?phrase=`,400);
  });
  
  function ExpectedRequest(pageName, expect){
      return request(app)
      .get(pageName)
      .set('Accept', 'application-json')
      .expect(expect);
  }
});
