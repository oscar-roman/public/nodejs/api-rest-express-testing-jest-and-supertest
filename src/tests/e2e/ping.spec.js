const request = require('supertest');
const { createServer } = require('../../server');
const app = require('../../index');

describe('GET /ping', () => {
  test('200 OK with "pong" body is returned', async () => {
    await request(app)
      .get('/ping')
      .expect(200)
      .expect('Content-Type', 'text/plain; charset=utf-8')
      .expect('pong');
  });
});
