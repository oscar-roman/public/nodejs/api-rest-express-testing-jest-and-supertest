const { contactFixture } = require('../fixtures');
const { fakeDatabase } = require('../../database/fakeDatabase');
const request = require('supertest');
const app = require('../../index');

describe('GET /contacts/<contact-id>', () => {
  beforeEach(() => {
    fakeDatabase.clear();
  });

  test('get Contact by its ID', async () => {
    // given
    const contact = contactFixture({
      id: '123',
      name: 'Contact 123',
      phone: '0-123-456-789',
      addressLines: ['Fancy Street', 'Mega City', 'What-a-Country'],
    });
    fakeDatabase.insertIntoContacts(contact);

    ExpectedRequest('/contacts/123',contact)
  });

  describe('get non-existent Contact by its ID', () => {
    test('HTTP status', async () => {
      // given
      fakeDatabase.insertIntoContacts(contactFixture({ id: '123' }));
      ExpectedRequest('/contacts/987',404);
    });
  });
});

function ExpectedRequest(pageName, expect){
  return request(app)
  .get(pageName)
  .set('Accept', 'application-json')
  .expect(expect);
}